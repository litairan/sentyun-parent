package com.sentyun.cloud.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentyunNacosServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentyunNacosServerApplication.class, args);
    }

}
